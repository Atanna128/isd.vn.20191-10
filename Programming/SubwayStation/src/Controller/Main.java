package Controller;

import hust.soict.se.customexception.InvalidIDException;
import  hust.soict.se.scanner.CardScanner;
public class Main {

    public static void main(String[] args) throws InvalidIDException {
        String pseudoBarCode = "ABCDEFGH";
        CardScanner cardScanner = CardScanner.getInstance();
        String cardId = cardScanner.process(pseudoBarCode);
        System.out.println(cardId);
    }
}
