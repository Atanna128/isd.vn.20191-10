## Work assignment in Architecture Design 
### Communication Diagram && Class Diagram
#### Leader : Nguyễn Duy Ý
### Nguyễn Duy Ý

1. fix sequence diagram after reviewed (Exit station by one-way ticket diagram)
2. Draw Exit station by one-way ticket communication diagram
3. Draw Exit station by one-way ticket class diagram
### Bùi Ngọc Tú
1. Fix sequence diagram after reviewed ( Enter station by one-way ticket diagram and Enter station by prepaid card diagram)
2. Draw Enter station by one-way ticket communication diagram
3. Draw Enter station by prepaid card communication diagram
4. Draw Enter station by one-way ticket class diagram
5. Draw Enter station by prepaid card class diagram
6. Combine class diagram for entire system.
6. Reformat members's file and summarize homework
